﻿
import java.util.Scanner;


/*Aufgabe 3: Vorteil der Implementierung von Arrays ist, dass man so größere Datenmengen in einer einzigen Variable bzw. Liste verwalten kann, statt in 
 *  mehreren einzelnen Datentypen. Besonders in dieser Aufgabe zahlt sich dies aus, da man nun mehrere verschiedene Tickettypen mit unterschiedlichen 
 *  Preisen hat, hätte man diese in einzelne Variablen gespeichert, wäre der Quellcode schnell unübersichtlich geworden. Zusätzlich durch die neue Implentierung, hat
 *  man später kürzere Zugriffszeiten, wenn man noch Ticketarten und Preise ergänzen möchte. Nachteil ist, wenn solche Arraylisten zu groß werden, kann man auch hier
 *  die Übersicht verlieren, besonders wenn dann ein Element an einer bestimmten Stelle verändert werden soll, und die Zugriffszeiten verlängern sich dann dementsprechend.
 *  Ein Nachteil ist, dasss wenn man eine bestimmte Position eines Elements eines Arrays abändert, müssen auch alle Stellen im Quellcode abgeändert werden, an denen dieses
 *  Element angesprochen wurde, weil alle Elemente des Arrays feste Positionen haben.
 *  */

public class Fahrkartenautomat {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub	      
		
			while(0 < 1) {
				
				
			       // Variable speichert den return/Rückgabewert
			       double zuZahlenderBetrag = fahrKartenbestellungErfassen();
			       
			       double rückgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			       
			       // bei void muss nichts gespeichert werden, weil kein Rückgabewert
			       fahrkartenAusgeben();
			       
			       rueckgeldAusgeben(rückgabeBetrag);  
				
			}
     
	       
	}
	
	//Beginn der Methoden
	    
    public static double fahrKartenbestellungErfassen() {
    	
    		byte ticketanzahl = 0;
    		double zuZahlenderBetrag = 0;
    		int fahrkartenwahl= 0;
    		//double einzelfahrschein = 2.90;
    		//double tageskarte = 8.60;
    		//double kleingruppe = 23.50;
    	
    		String [ ] bezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC" };
    	
    		double [ ] preis = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    		
    		Scanner tastatur = new Scanner(System.in);
    		
    		System.out.println("Guten Tag, bitte wählen Sie eine Fahrkarte aus: \n");
    		
    		for(int i = 0; i < bezeichnung.length; i++) {	
    			
    		System.out.printf("%s%s%.2f%s", bezeichnung[i], " zu ",  preis[i], " €\n"); 	
    			
    		}
    	    
    	   System.out.println("\nIhre Wahl: "); 
    	   
    	   fahrkartenwahl = tastatur.nextInt();
    	   
    	   int ticketwahl = fahrkartenwahl -1;
    	   
    	   zuZahlenderBetrag = preis [ticketwahl]; 
	       
	       //Eingabe der Ticketanzahl
	       System.out.print("Anzahl der Tickets: ");
	       ticketanzahl = tastatur.nextByte();
	       
	       
	       //Ticketpreis multipliziert mit der Ticketanzahl
	       if (ticketanzahl > 1)
	       {
	    	   zuZahlenderBetrag = zuZahlenderBetrag * ticketanzahl;
	       }
	       
	       return zuZahlenderBetrag;
	  
    }
    
 
    // hier kein void nehmen, weil wir den rückgabebetrag brauchen
    public static double fahrkartenBezahlen(double zuZahlen) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	double rückgabebetrag;

    	
    	eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlen)
	       {	
	    	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlen - eingezahlterGesamtbetrag), " EURO");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
    	
    	return rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
    	}
    
    //void weil nichts berechnet werden muss, sondern es wird einfach ausgeführt
    public static void fahrkartenAusgeben() {
    	
    	
    	System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	          
	       }
	       System.out.println("\n\n");
    	}
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	
    	if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("%s%.2f%s\n", "Der Rückgabebetrag in Höhe von ", (rückgabebetrag), " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	          
	       	}

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n");
			}
    	
    	
    }
    