import java.util.Scanner;
public class Taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
/* Der Benutzer soll zwei Zahlen in das Programm eingeben, danach soll er entscheiden, ob die 
Zahlen addiert, subtrahiert, multipliziert oder dividiert werden, diese Entscheidung soll �ber die 
Eingabe der folgenden Symbole get�tigt werden: +, -, *, /
Nach der Auswahl soll das Ergebnis der Rechnung ausgegeben werden, bzw. eine 
Fehlermeldung, falls eine falsche Auswahl getroffen wurde*/
		
	Scanner myScanner = new Scanner(System.in);
	
	System.out.println("Bitte geben Sie die erste Zahl ein: ");
	
	double zahl1 = myScanner.nextDouble();
	
	System.out.println("Bitte geben Sie die zweite Zahl ein: ");

	double zahl2 = myScanner.nextDouble();	
		
	System.out.println("Sollen die Zahlen addiert [+], subtrahiert [-], multipliziert [*], oder dividiert [/] werden? \n\n"
			+ "Geben Sie das entsprechende Zeichen ein: ");	
	
	char zeichen = myScanner.next().charAt(0);
	
	if (zeichen == '+') {
		
	double ergebnis = zahl1 + zahl2;
	
	System.out.printf("Das Ergebnis der Addition lautet: " + ergebnis);
		
	} else if (zeichen == '-') {
		
	double ergebnis = zahl1 - zahl2;
	
	System.out.println("Das Ergebnis der Subtraktion lautet: " + ergebnis);
		
	} else if (zeichen == '*') {
		
	double ergebnis = zahl1 * zahl2;
	
	System.out.println("Das Ergebnis der Multiplikation lautet: " + ergebnis);
		
	} else if (zeichen == '/') {
		
	double ergebnis = zahl1 / zahl2;
	
	System.out.printf("%s%.2f", "Das Ergebnis der Division lautet: ", + ergebnis);
		
	} else {System.out.println("Dies war eine falsche Eingabe");}
	} 

}
