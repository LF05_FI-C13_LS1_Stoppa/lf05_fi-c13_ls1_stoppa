import java.util.Scanner;
public class Matrix {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int zahl = myScanner.nextInt();
		int i = 0;
		
		System.out.println("");
		
		while (i < 100) {
			
			int ersteStelle = i / 10;
			int zweiteStelle = i % 10;
			
			
			if (i % zahl == 0) // Pr�ft die Teilbarkeit
				System.out.printf("%4s", "*");
			
			else if ( ersteStelle == zahl || zweiteStelle == zahl) // Pr�ft, ob die Zahl an einer Stelle vorkommt
				System.out.printf("%4s", "*");
			
			else if ( ersteStelle + zweiteStelle == zahl) // Pr�ft, ob die Zahl der Quersumme entspricht
				System.out.printf("%4s", "*");
			
			else 
				System.out.printf("%4d", i);
			
			i++;
			
			if (i % 10 == 0 && i != 0) // Sorgt f�r Umbruch am Zeilenende
				System.out.println();
	
		}


	}



}
