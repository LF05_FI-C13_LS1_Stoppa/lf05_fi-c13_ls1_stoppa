import java.util.Scanner;
public class OhmschesGesetz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/* Nach dem Ohmschen Gesetz berechnet sich der Widerstand eines ohmschen Widerstandes 
		mit:

 		Schreiben Sie ein Programm, in das der Benutzer zun�chst �ber die Eingabe der 
		Buchstaben R, U oder I ausw�hlen kann, welche Gr��e berechnet werden soll. Gibt er einen 
		falschen Buchstaben ein, soll eine Meldung �ber die Fehleingabe erfolgen.
		Anschlie�end soll er die Werte der fehlenden Gr��en eingeben. Am Ende gibt das 
		Programm den Wert der gesuchten Gr��e mit der richtigen Einheit aus. */
	
		
	char zeichen;
	
	double r, u, i;
		
	Scanner myScanner = new Scanner(System.in);	
	
    System.out.println("Welche Gr��e soll berechnet werden: Widerstand [R], Spannung [U], oder Stromst�rke [I]?\n\n"
    		+ "Bitte w�hlen Sie das enstsprechende Zeichen ein: ");
    
	zeichen = myScanner.next().charAt(0);
	
	if (zeichen == 'R' || zeichen == 'r') {
		
	System.out.println("Geben Sie eine Zahl f�r die Spannung [U] ein: ");
	
	double zahl1 = myScanner.nextDouble();
	
	System.out.println("Geben Sie eine zweite Zahl f�r die Stromst�rke [I] ein: ");
	
	double zahl2 = myScanner.nextDouble();
	
	r = zahl1 / zahl2;
	
	System.out.printf("%s%.2f%s", "R = ", + r, " Ohm");
		
		
	}
	
	else if (zeichen == 'U' || zeichen == 'u') {
		
	System.out.println("Geben Sie eine Zahl f�r den Widerstand [R] ein: ");	
	
	double zahl1 = myScanner.nextDouble();
	
	System.out.println("Geben Sie eine zweite Zahl f�r die Stromst�rke [I] ein: ");
	
	double zahl2 = myScanner.nextDouble();
	
	u = zahl1 * zahl2;
	
	System.out.printf("%s%.2f%s", "U = ", + u, " Volt");
	
	}
	
	else if(zeichen == 'I' || zeichen =='i') {
	
		System.out.println("Geben Sie eine Zahl f�r die Spannung [U] ein: ");	
		
		double zahl1 = myScanner.nextDouble();
		
		System.out.println("Geben Sie eine zweite Zahl f�r den Widerstand [R] ein: ");
		
		double zahl2 = myScanner.nextDouble();
		
		i = zahl1 / zahl2;	
		
		System.out.printf("%s%.2f%s", "I = ", + i, " Ampere");
	}
	
	else {System.out.println("Das Zeichen ist ung�ltig.");}
		
		
		
	}
	
	
	


}
